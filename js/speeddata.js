
var tooltipContent=0;

$(document).ready(function() {
    $.ajaxSetup({ cache: false });
    var speedimg = chrome.extension.getURL('../img/speedmirafloresentucorazon.png');
    var speedclose = chrome.extension.getURL('../img/speedclose.png');
    // var iconCalendar = chrome.extension.getURL('../img/calendar.png');
    // var iconTextEdit = chrome.extension.getURL('../img/text-edit.png');
    // var iconTours = chrome.extension.getURL('../img/tours.png');
    $("#leftop > a").fadeOut(1000);
    $("#leftop").prepend('<div id="logodata">'+
            '<a>'+
            '<img src="'+speedimg+'"/> '+
            '</a>'+
        '</div>'
    );
    $("#logodata").click(function(){
        $("#bodi-content-centro > .main_view").fadeOut(100);
        if($("#speedcaja").length==0 || $("#speedcaja").length==undefined){
            $("#bodi-content-centro").append('<div id="speedcaja">'+
                    '<div class="speedclose"><img src="'+speedclose+'"></div>'+
                    '<h2>CATEGORIAS</h2>'+
                    '<ul class="list-cat">'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9047/administracion/" target="_blank"><div class="margin1 cat1"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tadmin.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9034/locales-comerciales/" target="_blank"><div class="margin1 cat2"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tcomercio.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9044/educacion/" target="_blank"><div class="margin1 cat3"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tcultura.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9056/licencia-de-edificacion/" target="_blank"><div class="margin1 cat4"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tedificacion.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9029/fiscalizacion-y-control/" target="_blank"><div class="margin1 cat5"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tfisca.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9050/obras/" target="_blank"><div class="margin1 cat6"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tobras.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9049/presupuesto/" target="_blank"><div class="margin1 cat7"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tpresupuesto.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/8833/demograficas/" target="_blank"><div class="margin1 cat8"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tregistrocivil.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/9057/licencia-de-transito/" target="_blank"><div class="margin1 cat9"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/ttransito.png" alt=""></div></a></li>'+
                        '<li><a href="http://datosabiertos.miraflores.gob.pe/dashboards/8964/turismo-y-gastronomia/" target="_blank"><div class="margin1 cat10"></div><div class="text-height"><img src="https://www.miraflores.gob.pe/datosabiertos/datosabiertos/images/textos/tturismo.png" alt=""></div></a></li> '+
                    '</ul>'+
                '</div>'
            );            
        }else{
            $("#bodi-content-centro > #speedcaja").fadeIn(1000);
        }
        $(".speedclose > img").click(function(){
            $("#bodi-content-centro > #speedcaja").fadeOut(100);
            $("#bodi-content-centro > .main_view").fadeIn(1000);
        });  
        // if(tooltipContent==0){            
        //     $("#tool").addClass("tooltip-click");
        //     tooltipContent=1;
        // }else{
        //     $("#tool").removeClass("tooltip-click");
        //     tooltipContent=0;
        // }       
    });
       

// $("#leftop").prepend('<div id="tool" class="tooltip">'+
//             '<img id="MiCongreso" src="'+iconCongreso+'"/> '+
//             '<span class="tooltip-content">'+
//                 '<span class="tooltip-text">'+
//                     '<span class="tooltip-inner">'+
//                         '<div class="caja">'+
//                             '<ul class="itemas" style="width: 470px;">'+
//                                 '<li class="itema">'+
//                                     '<a class="linkli" href="http://www.congreso.gob.pe/sintesisacuerdoscomisiones" target="_blank">'+
//                                         '<img class="icoco" src="'+iconTextEdit+'">'+
//                                             '<span class="txt">Acuerdos</span></a></li>'+
//                                 '<li class="itema">'+
//                                     '<a class="linkli" href="http://www.congreso.gob.pe/sintesisagendascomisiones" target="_blank">'+
//                                         '<img class="icoco" src="'+iconAgenda+'">'+
//                                         '<span class="txt">Agendas</span></a></li>'+
//                                 '<li class="itema">'+
//                                     '<a class="linkli" href="http://www.congreso.gob.pe/sintesisagendascomisiones" target="_blank">'+
//                                         '<img class="icoco" src="'+iconCalendar+'">'+
//                                         '<span class="txt">Calendar</span></a></li>'+
//                                 '<li class="itema">'+
//                                     '<a class="linkli" onclick="" href="#">'+
//                                         '<img class="icoco" src="'+iconTours+'">'+
//                                         '<span class="txt">Congresistas</span></a></li>'+
//                             '</ul>'+
//                         '</div>'+
//                     '</span>'+
//                 '</span>'+
//             '</span>'+
//         '</div>'
//     );
    
    
});

